import argparse
import sys
import time
from Problem import *

def main():
    parser = argparse.ArgumentParser(description='''The driver Script for the Explanation generation''',
                                     epilog="Usage >> ./Explainer.py -d ../domain/original_domain.pddl -p" +
                                            " ../domain/original_problem.pddl -f ../domain/foil.sol")
    '''
        # Flags
        --generate_lattice
    '''

    parser.add_argument('-m', '--domain_model',   type=str, help="Domain file with real PDDL model of robot.", required=True)
    parser.add_argument('-n', '--human_domain_model',   type=str, help="Domain file with real PDDL model of robot.", required=True)
    parser.add_argument('-p', '--problem', type=str, help="Problem file for robot.", required=True)
    parser.add_argument('-q', '--human_problem', type=str, help="Problem file for robot.", required=True)
    parser.add_argument('-t', '--domain_templ', type=str, help="Problem file for robot.", required=True)
    parser.add_argument('-r', '--prob_templ', type=str, help="Problem file for robot.", required=True)
    parser.add_argument('-H', '--heuristic', type=str, help="Problem file for robot.", required=True)


    if not sys.argv[1:] or '-h' in sys.argv[1:]:
        print (parser.print_help())
        sys.exit(1)
    args = parser.parse_args()
    

    problem = Problem(args.domain_model, args.human_domain_model, args.problem, args.human_problem, args.heuristic, args.domain_templ, args.prob_templ)
    st_time = time.time()
    plan = problem.plan()
    cnt=0
    for p in plan:
        if 'explain' in p:
            cnt += 1
    print ("Plan >>", plan)
    print ("Total time >>",time.time() - st_time)
    print ("Explanation Size >>",cnt)
if __name__ == "__main__":
    main()
