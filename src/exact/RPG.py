import sys
import os
import copy
INIT_PRED = "initial_state"


MAX_UNROL = 100

class RelaxedPlanGraph:
    def __init__(self, dom_map):
        self.dom_map = dom_map
    
    def get_h_max_from_state(self, curr_state, DEBUG = False):
        curr_level = 0
        rpg_graph = []
        act_layers = []
        rpg_graph.append(curr_state)
        search_for_max_level = True
        curr_fact_layer = copy.deepcopy(rpg_graph[-1])
        not_yet_init_action = False
        #if INIT_PRED in curr_state:
        #    not_yet_init_action = True


        if self.dom_map["problem"]["goal"] <= curr_fact_layer:
                search_for_max_level = False
                #if not not_yet_init_action:
                return curr_level
                #else:
                #    return (curr_level - 1)*10 + 1

        if DEBUG:
           print (curr_state)
        if 'NAN' in curr_state:
            return -2
        while search_for_max_level:
            curr_level += 1
            curr_fact_layer = copy.deepcopy(rpg_graph[-1])
            curr_act = set()
            for act in self.dom_map['domain']:
                prec = set()
                for p in self.dom_map['domain'][act]['precondition_pos']:
                    prec.add(p)
                if 'precondition_pos_cond' in self.dom_map['domain'][act]:
                    for pr in self.dom_map['domain'][act]['precondition_pos_cond']:
                        pr_parts = pr.split('@')
                        if 'should_know_' in pr_parts[0] and pr_parts[0] in rpg_graph[-1]:
                            prec.add(pr_parts[1])

                if prec <= rpg_graph[-1]:
                    if DEBUG:
                        print (act, self.dom_map['domain'][act]['effect_pos'])
                    for pred in self.dom_map['domain'][act]['effect_pos']:
                        curr_fact_layer.add(pred)
                    if 'effect_pos_cond' in self.dom_map['domain'][act]:
                       for eff in self.dom_map['domain'][act]['effect_pos_cond']:
                            eff_parts = eff.split('@')
                            if eff_parts[0] in rpg_graph[-1]:
                                curr_fact_layer.add(eff_parts[1])
                    curr_act.add(act)

            if self.dom_map["problem"]["goal"] <= curr_fact_layer:
                search_for_max_level = False
                #if not not_yet_init_action:
                return curr_level #*10
                #else:
                #    return (curr_level - 1)*10 + 1
                #return curr_level

            act_layers.append(curr_act)
            if curr_fact_layer == rpg_graph[-1] or curr_level >= MAX_UNROL:
                search_for_max_level = False
                if DEBUG:
                    print ("Here Here",curr_fact_layer == rpg_graph[-1], curr_level)
                return -1
            rpg_graph.append(curr_fact_layer)
        return -1
