from queue import PriorityQueue, Queue
from functools import total_ordering
import itertools
from RPG import *
import copy
import time
import math

UNIQUE_SEEN = False
SKIP_STATE = 'NAN'
GOAL_PLAN = ['putdown*A', 'INIT_ACT', 'unstack*A*D','putdown*C', 'pickup*D']


def unique_everseen(iterable, key=None):
    '''
     Recipe taken from https://docs.python.org/3/library/itertools.html#itertools-recipes
    '''
    "List unique elements, preserving order. Remember all elements ever seen."
    # unique_everseen('AAAABBBCCDAABBB') --> A B C D
    # unique_everseen('ABBCcAD', str.lower) --> A B C D
    seen = set()
    seen_add = seen.add
    if key is None:
        for element in filterfalse(seen.__contains__, iterable):
            seen_add(element)
            yield element
    else:
        for element in iterable:
            k = key(element)
            if k not in seen:
                seen_add(k)
                yield element


def hamming_dist(curr_state, goal):
    dist = 0
    for pred in goal:
        if pred in curr_state:
            dist += 1
    #return dist
    return 0 #dist

def hmax(rpg_obj, curr_state, goal, DEBUG = False):
    h = rpg_obj.get_h_max_from_state(curr_state , DEBUG)
    #if h == -1:
    #    print ("Problem not solvable!!!")
    #    exit(1)
    return h


def astarSearch(start_state, goal_state, problem):
    rpg = RelaxedPlanGraph(problem.meta_model)
    fringe = PriorityQueue()
    closed = set()
    nodes_expanded = 0
    fringe.put((0,start_state))

    while not fringe.empty():
        
        val, node = fringe.get()#[1]

        #print ("Popped node",node[1],val)

        if node != None:
            goal_stat = problem.goal_test(node)
            if goal_stat:
                print ("Goal Found! Number of Nodes Expanded =", nodes_expanded, "cost=",val)
                return node #.get_plan()
            if UNIQUE_SEEN:
                closed_key = unique_everseen(node[0])
            else:
                closed_key = frozenset(node[0])
            if closed_key not in closed:
                #print (closed)
                closed.add(closed_key)
                successor_list = problem.find_all_successors(node)
                nodes_expanded += 1
    
                if not nodes_expanded % 100:
                    print ("Number of Nodes Expanded =", nodes_expanded)
                #print (successor_list)
                #HEUR = []
                while successor_list: 
                    candidate_node = successor_list.pop()
                    heur = hmax(rpg, candidate_node[0],goal_state)
                    if heur != -1:
                        cost = 0
                        for act in candidate_node[1]:
                            cost += problem.find_action_cost(act,candidate_node)

                        fringe.put((cost + heur, candidate_node))
                        #HEUR.append([cost + heur,candidate_node[1][-1]])
                    else:
                        #print ("ignoring paths",candidate_node[1])#, hmax(rpg, candidate_node[0],goal_state, True))
                        if candidate_node[1][-1] == 'move_from_x_to_y*p1*p7' and 'STOP_CHECKING_BUDGET' in candidate_node[1]:
                            print (hmax(rpg, candidate_node[0],goal_state, True))
                #print (HEUR)
                #exit(0)
    return None



def repeated_search(start_state, goal_state, problem, curr_model_map, last_fringe , last_plan_node , old_plan_size=None):
    # unlike the other search here the nodes are marked uncertain

    rpg = RelaxedPlanGraph(problem.optimistic_model)
    open_list = PriorityQueue()
    open_list_bk = PriorityQueue()
    invalid_list = PriorityQueue()
    closed = set()
    visited_but_not_expanded = PriorityQueue()
    nodes_expanded = 0
    if not last_fringe:
        open_list.put((0,start_state))
    else:
        Fringe_Print = False
        new_plan_node = problem.revaluate_node(last_plan_node, curr_model_map)
        if 'NAN' not in new_plan_node[0]:
            return last_plan_node, last_fringe


        #ent = input('Look at the fringe')
        #if ent == 'y':
        #   Fringe_Print = True 
        #print ("open list size",last_fringe[0].qsize())
        #print ("invalid list size",last_fringe[1].qsize())
        #old_open_list = copy.deepcopy(last_fringe[0])
        old_open_list = last_fringe[0]
        #old_invalid_list = copy.deepcopy(last_fringe[1])
        old_invalid_list = last_fringe[1]
        old_plan_node = copy.deepcopy(last_plan_node)
        while not old_open_list.empty():
            val, node = old_open_list.get()
            if Fringe_Print and node[1][:5] == GOAL_PLAN:
                print (node[1])
            node[-1] = "UNCERT"
            open_list.put((val, node))
            open_list_bk.put((val, node))
        while not old_invalid_list.empty():
            val, node = old_invalid_list.get()
            if Fringe_Print: # and node[1][:5] == GOAL_PLAN:
                print ("inv", node[1])
            node[-1] = "UNCERT"
            open_list_bk.put((val, node))
        old_plan_node[-1] = "UNCERT"
        cost = 0
        for act in old_plan_node[1]:
            cost += problem.find_action_cost(act,old_plan_node,human_test=True)
        open_list_bk.put((cost,old_plan_node))

    #print ("OPEN_LIST SIZE",open_list.qsize())

    while not open_list.empty():
#        print ("closed",closed)
        val, node = open_list.get()
        #print ("NODE FOR SUCC",node[1])
        goal_stat = problem.human_goal_test(node, curr_model_map)
        if goal_stat:
            print ("Goal Found! Number of Nodes Expanded =", nodes_expanded, node[1], "cost=",val)
            #if  not last_fringe:
            while not visited_but_not_expanded.empty():
                a,b =visited_but_not_expanded.get()
                open_list.put((a,b))
            #        a,b = open_list_bk.get()
            #        print ("FIRST FRINGE...",b[1])
            #    while not invalid_list.empty():
            #        a,b = invalid_list.get()
            #        print ("FIRST FRINGE...",b[1])
            #exit(0)
            # Should I be changing fringes in between the search 
            return node, [open_list, invalid_list]
        #else:
        #    print ("Goal not Found! for ", node[1])
        #if UNIQUE_SEEN:
        #    closed_key = unique_everseen(node[0])
        #else:
        closed_key = ("_".join(sorted(list(node[0]))))
        #if frozenset(node[0]) not in closed:
        if closed_key not in closed or SKIP_STATE in node[0]:
            successor_list, invalid_succ = problem.find_all_fringe_elements(node, curr_model_map)
            #print (node[0],node[1],[i[1][-1] for i in successor_list])
            #print ("invalid_succ size",len(invalid_succ))
            #print ("succ size",len(successor_list))
            if len(successor_list) > 0:
                closed.add(closed_key)
            nodes_expanded += 1

            if not nodes_expanded % 100:
                print ("Number of Nodes Expanded in secondary search = ", nodes_expanded)
            #print (node[1])

            while successor_list:
                candidate_node = successor_list.pop()
                heur = hmax(rpg, candidate_node[0],goal_state)
                #print ("SUCC",candidate_node[1])
                if heur >=0:
                    cost = 0
                    for act in candidate_node[1]:
                        cost += problem.find_action_cost(act,candidate_node,human_test=True)
                    open_list.put((cost + heur, candidate_node))
                elif heur ==-2:
                    #print (candidate_node[0],node[1],candidate_node[1])
                    invalid_list.put((val+problem.find_action_cost(candidate_node[1][-1],node) , candidate_node))
                    exit(0)
                else:
                    #print ("IMPOSSIBLE PLANS",candidate_node[1])
                    invalid_list.put((val+problem.find_action_cost(candidate_node[1][-1],node) , candidate_node))
            #print ("Open set size", open_list.qsize())
            while invalid_succ:
                candidate_node = invalid_succ.pop()
                #invalid_list.put((len(candidate_node[1]) + hmax(rpg, candidate_node[0],goal_state), candidate_node))
                #print ("INV SUCC",candidate_node[1])
                cost = 0
                old_cost = 0
                for act in candidate_node[1]:
                    cost += problem.find_action_cost(act,candidate_node,human_test=True)
                for act in node[1]:
                    old_cost += problem.find_action_cost(act,node,human_test=True)

                invalid_list.put((val+ (cost - old_cost) , candidate_node))
        else:
            visited_but_not_expanded.put((val,node))
            #print ("Already in closed",node[1])
    print ("Going to print invalid_list",invalid_list.qsize())
    while not open_list_bk.empty():
        a,b = open_list_bk.get()
        print (b[1])
    #print ("Printed invalid_list")
    #return last_plan_node, last_fringe
    # This can't happen in the normal scenario
    return None

