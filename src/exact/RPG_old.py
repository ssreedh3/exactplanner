import sys
import os
import copy

MAX_UNROL = 100

class RelaxedPlanGraph:
    def __init__(self, dom_map):
        self.dom_map = dom_map
    
    def get_h_max_from_state(self, curr_state, DEBUG = False):
        #DEBUG = True
        curr_level = 0
        rpg_graph = []
        act_layers = []
        rpg_graph.append(curr_state)
        search_for_max_level = True
        curr_fact_layer = copy.deepcopy(rpg_graph[-1])
        if self.dom_map["problem"]["goal"] <= curr_fact_layer:
                search_for_max_level = False
                #print ("rpg",rpg_graph,curr_fact_layer)
                return curr_level

        #print ("INIT_ACT",self.dom_map['domain']['INIT_ACT'])
        #print ("##############################################################################")
        if DEBUG:
           print (curr_state)
        #exit(0)
        if 'NAN' in curr_state:
            #print ("No No Here Here")
            #exit(0)
            return -2
        while search_for_max_level:
            curr_level += 1
            curr_fact_layer = copy.deepcopy(rpg_graph[-1])
            #print (rpg_graph)
            curr_act = set()
            for act in self.dom_map['domain']:
                prec = set()
                for p in self.dom_map['domain'][act]['precondition_pos']:
                    prec.add(p)
                if 'precondition_pos_cond' in self.dom_map['domain'][act]:
                    for pr in self.dom_map['domain'][act]['precondition_pos_cond']:
                        pr_parts = pr.split('@')
                        if 'should_know_' in pr_parts[0] and pr_parts[0] in rpg_graph[-1]:
                            prec.add(pr_parts[1])
                    #print ("prec",prec)
                    #exit(0)


                if prec <= rpg_graph[-1]:
                    if DEBUG:
                        print (act, self.dom_map['domain'][act]['effect_pos'])
                    for pred in self.dom_map['domain'][act]['effect_pos']:
                        curr_fact_layer.add(pred)
                    if 'effect_pos_cond' in self.dom_map['domain'][act]:
                       for eff in self.dom_map['domain'][act]['effect_pos_cond']:
                            eff_parts = eff.split('@')
                            if eff_parts[0] in rpg_graph[-1]:
                                curr_fact_layer.add(eff_parts[1])
                    curr_act.add(act)
            #if DEBUG:
            #print (curr_act)
            #print (curr_fact_layer - rpg_graph[-1])

            if self.dom_map["problem"]["goal"] <= curr_fact_layer:
                search_for_max_level = False
                #print ("rpg",rpg_graph,curr_fact_layer)
                return curr_level
            act_layers.append(curr_act)
            if curr_fact_layer == rpg_graph[-1] or curr_level >= MAX_UNROL:
                search_for_max_level = False
                if DEBUG:
                    print ("Here Here",curr_fact_layer == rpg_graph[-1], curr_level)
                #exit(0)
                #rpg_graph.append(curr_fact_layer)
                #for ly in rpg_graph:
                #    print (ly,"\n")
                return -1
            rpg_graph.append(curr_fact_layer)
        return -1
