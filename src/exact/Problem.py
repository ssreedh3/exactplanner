import sys
import os
import yaml
import time
import pddlpy
import copy
from Search import *
PLANNER_COMMAND = './fdplan.sh {} {}'
VAL_COMMAND = './valplan.sh {} {} {}'
ACTION_DEF_STR = '(:action {}\n:parameters ({})\n:precondition\n(and\n{}\n)\n:effect\n(and\n{}\n)\n)\n'
BELIEF_PREFIX = "belief_"
INIT_PRED = "initial_state"
GOAL_PRED = "goal_achieved"
SKIP_STATE = 'NAN'
LAST_HUMAN_FRINGE = None
LAST_HUMAN_PLAN = None
orig_time = time.time()

HEUR_MAP = {
        'ham': hamming_dist,
        'hmax': hmax
        }

MODEL_PARTS = ['precondition_pos', 'precondition_neg', 'effect_pos', 'effect_neg']
#TODO: Make sure there are no hiphens in action name or use other connectors

OPTIMALITY_TEST_MAP = {}

class Problem:

    def __init__(self, robot_domain_model, human_domain_model,robot_problem, human_problem, heuristic, domain_template, prob_template):
        
        self.heuristic_func = HEUR_MAP[heuristic]

        robot_dom_prob = pddlpy.DomainProblem(robot_domain_model, robot_problem)
        self.orig_start = self.convert_prop_tuple_list(robot_dom_prob.initialstate())
        self.goal_state = self.convert_prop_tuple_list(robot_dom_prob.goals())
        self.grounded_robot_model_map = self.convert_domain_obj_map(robot_dom_prob)
        self.domain_template = domain_template
        self.prob_template = prob_template

        human_dom_prob = pddlpy.DomainProblem(human_domain_model, human_problem)
        self.orig_start = self.convert_prop_tuple_list(human_dom_prob.initialstate())
        self.goal_state = self.convert_prop_tuple_list(human_dom_prob.goals())
        self.grounded_human_model_map = self.convert_domain_obj_map(human_dom_prob)

        #print (self.grounded_human_model_map['domain'].keys())
        #exit(0)

        self.meta_model = self.get_meta_model(self.grounded_robot_model_map, self.grounded_human_model_map)
        self.optimistic_model = self.get_optimistic_model(self.grounded_robot_model_map, self.grounded_human_model_map)
        #print (self.meta_model)

        with open(domain_template) as d_fd:
            self.domain_template_str = d_fd.read().strip()
        with open(prob_template) as p_fd:
            self.prob_template_str = p_fd.read().strip()



    def convert_prop_tuple_list(self, orig_prop_list):
        prop_list = set()
        for p in orig_prop_list:
            #print (p)
            if type(p) is tuple:
                prop = ' '.join([str(i).lower() for i in p])
            else:
                prop = ' '.join([str(i).lower() for i in p.predicate])
            #print ("prop",prop)
            prop_list.add(prop)
        return prop_list 

    def convert_domain_obj_map(self, prob_object):
        dom_map = {}
        dom_map['domain'] = {}
        ground_operator = self.ground_all_operators(prob_object)
        act_applied_preds = set()
        for act in ground_operator:
            sorted_var_name = list(act.variable_list.keys())
            sorted_var_name.sort()
            objs = [act.variable_list[k] for k in sorted_var_name]            

            action_name = act.operator_name + '*' + '*'.join(objs) 
            dom_map['domain'][action_name] = {}
            dom_map['domain'][action_name]['precondition_pos'] = self.convert_prop_tuple_list(act.precondition_pos)
            dom_map['domain'][action_name]['precondition_pos'].add("APPLIED_INIT_ACT")
            dom_map['domain'][action_name]['precondition_pos_cond'] = set()
            dom_map['domain'][action_name]['precondition_neg'] = self.convert_prop_tuple_list(act.precondition_neg)
            dom_map['domain'][action_name]['effect_pos'] = self.convert_prop_tuple_list(act.effect_pos)
            dom_map['domain'][action_name]['effect_neg'] = self.convert_prop_tuple_list(act.effect_neg)
            dom_map['domain'][action_name]['effect_neg'].add("APPLIED_"+act.operator_name)
            act_applied_preds.add("APPLIED_"+act.operator_name)
            dom_map['domain'][action_name]['effect_pos_cond'] = set()
            dom_map['domain'][action_name]['effect_neg_cond'] = set()

        dom_map['problem'] = {}
        dom_map['problem']['init'] = set() #copy.deepcopy(self.orig_start)
        dom_map['problem']['init'].add(INIT_PRED)
        for pred in act_applied_preds:
            dom_map['problem']['init'].add(pred)

        dom_map['problem']['goal'] = set() #copy.deepcopy(self.goal_state)
        dom_map['problem']['goal'].add(GOAL_PRED)

        # Make a new initial_state action
        action_name = 'INIT_ACT'
        dom_map['domain'][action_name] = {}
        dom_map['domain'][action_name]['precondition_pos'] = set()
        dom_map['domain'][action_name]['precondition_pos'].add(INIT_PRED)
        dom_map['domain'][action_name]['precondition_pos_cond'] = set()
        dom_map['domain'][action_name]['precondition_neg'] = set()
        dom_map['domain'][action_name]['effect_pos'] = copy.deepcopy(self.orig_start)
        dom_map['domain'][action_name]['effect_pos'].add("APPLIED_INIT_ACT")

        dom_map['domain'][action_name]['effect_neg'] = set()
        dom_map['domain'][action_name]['effect_neg'].add(INIT_PRED)

        for pred in act_applied_preds:
            dom_map['domain'][action_name]['effect_neg'].add(pred)
        dom_map['domain'][action_name]['effect_pos_cond'] = set()
        dom_map['domain'][action_name]['effect_neg_cond'] = set()

        # Make a new goal action
        action_name = 'GOAL_ACT'
        dom_map['domain'][action_name] = {}
        dom_map['domain'][action_name]['precondition_pos'] = copy.deepcopy(self.goal_state)
        dom_map['domain'][action_name]['precondition_pos_cond'] = set()
        dom_map['domain'][action_name]['precondition_neg'] = set()
        dom_map['domain'][action_name]['effect_pos'] = set()
        dom_map['domain'][action_name]['effect_pos'].add(GOAL_PRED)
        dom_map['domain'][action_name]['effect_neg'] = set()
        dom_map['domain'][action_name]['effect_pos_cond'] = set()
        dom_map['domain'][action_name]['effect_neg_cond'] = set()


        return dom_map

    def get_optimistic_model(self, grounded_robot_model_map, grounded_human_model_map):
        optimistic_model = {}
        optimistic_model['domain'] = {}

        for action_name in grounded_human_model_map['domain'].keys():
            optimistic_model['domain'][action_name] = copy.deepcopy(grounded_human_model_map['domain'][action_name])
            optimistic_model['domain'][action_name]["precondition_pos"] = optimistic_model['domain'][action_name]["precondition_pos"] & grounded_robot_model_map['domain'][action_name]["precondition_pos"]
            optimistic_model['domain'][action_name]["effect_pos"] = optimistic_model['domain'][action_name]["effect_pos"] | grounded_robot_model_map['domain'][action_name]["effect_pos"]
        optimistic_model['problem'] = {}
        for p_key in grounded_human_model_map['problem'].keys():
            optimistic_model['problem'][p_key] = copy.deepcopy(grounded_robot_model_map['problem'][p_key])

        return optimistic_model


    def get_meta_model(self, grounded_robot_model_map, grounded_human_model_map):
        meta_dom_map = {}
        meta_dom_map['domain'] = {}
        meta_predicates_true_initial_state = set()
        meta_predicates_false_in_initial_state = set()

        for action_name in grounded_robot_model_map['domain'].keys():

            meta_dom_map['domain'][action_name] = {}
            meta_dom_map['domain'][action_name] = copy.deepcopy(grounded_robot_model_map['domain'][action_name])
            for k_parts in grounded_robot_model_map['domain'][action_name].keys():

                # For precondition or Deletes
                if k_parts == "precondition_pos" or k_parts == "effect_neg" or k_parts == "effect_pos":
                    human_additional_preds = grounded_human_model_map['domain'][action_name][k_parts] - grounded_robot_model_map['domain'][action_name][k_parts]
                    robot_additional_preds = grounded_robot_model_map['domain'][action_name][k_parts] - grounded_human_model_map['domain'][action_name][k_parts]
                    human_shared_preds = grounded_human_model_map['domain'][action_name][k_parts] - human_additional_preds #& grounded_robot_model_map['domain'][action_name][k_parts]

                    for pred in human_additional_preds:
                        new_meta_pred = "believes_#"+action_name.split('*')[0]+"#"+k_parts+"#"+pred.split(" ")[0]
                        new_pred = BELIEF_PREFIX + pred
                        meta_predicates_true_initial_state.add(new_meta_pred)
                        meta_dom_map['domain'][action_name][k_parts+"_cond"].add(new_meta_pred+'@'+new_pred)

                    for pred in robot_additional_preds:
                        new_meta_pred = "should_know_#"+action_name.split('*')[0]+"#"+k_parts+"#"+pred.split(" ")[0]
                        new_pred = BELIEF_PREFIX + pred
                        meta_predicates_false_in_initial_state.add(new_meta_pred)
                        meta_dom_map['domain'][action_name][k_parts+"_cond"].add(new_meta_pred+'@'+new_pred)

                    for pred in human_shared_preds:
                        new_pred = BELIEF_PREFIX + pred
                        meta_dom_map['domain'][action_name][k_parts].add(new_pred)


        #print ("meta_predicates_true_initial_state",meta_predicates_true_initial_state)
        #print ("meta_predicates_false_in_initial_state",meta_predicates_false_in_initial_state)
        #print ("meta_dom_map['problem'][p_key]",grounded_robot_model_map['problem']['init'])

        meta_dom_map['problem'] = {}
        for p_key in grounded_robot_model_map['problem'].keys():
            meta_dom_map['problem'][p_key] = copy.deepcopy(grounded_robot_model_map['problem'][p_key])
            for pred in grounded_human_model_map['problem'][p_key]:
                meta_dom_map['problem'][p_key].add(BELIEF_PREFIX+pred)
            if p_key == "init":
                for pred in meta_predicates_true_initial_state:
                    meta_dom_map['problem'][p_key].add(pred)


        # Add explanatory actions
        for pred in meta_predicates_true_initial_state:
            expl_act_name = "explain"+pred
            meta_dom_map['domain'][expl_act_name] = {}
            meta_dom_map['domain'][expl_act_name]['precondition_pos'] = set()
            act_name = pred.replace('should_know_','').replace("believes_",'').split('#')[1]
            #print (act_name,pred)
            #exit(0)
            meta_dom_map['domain'][expl_act_name]['precondition_pos'].add("APPLIED_"+act_name)
            meta_dom_map['domain'][expl_act_name]['precondition_pos'].add(BELIEF_PREFIX+"APPLIED_"+act_name)
            meta_dom_map['domain'][expl_act_name]['precondition_pos_cond'] = set()
            meta_dom_map['domain'][expl_act_name]['precondition_neg'] = set()
            meta_dom_map['domain'][expl_act_name]['effect_pos'] = set()
            meta_dom_map['domain'][expl_act_name]['effect_neg'] = set()
            meta_dom_map['domain'][expl_act_name]['effect_neg'].add(pred)
            meta_dom_map['domain'][expl_act_name]['effect_pos_cond'] = set()
            meta_dom_map['domain'][expl_act_name]['effect_neg_cond'] = set()

        for pred in meta_predicates_false_in_initial_state:
            expl_act_name = "explain"+pred
            #print ("adding actions",expl_act_name)
            meta_dom_map['domain'][expl_act_name] = {}
            meta_dom_map['domain'][expl_act_name]['precondition_pos'] = set()
            old_pred = pred
            act_name = old_pred.replace('should_know_','').replace("believes_",'').split('#')[1]
            #print ("APPLIED_"+act_name)

            meta_dom_map['domain'][expl_act_name]['precondition_pos'].add("APPLIED_"+act_name)
            meta_dom_map['domain'][expl_act_name]['precondition_pos'].add(BELIEF_PREFIX+"APPLIED_"+act_name)
            meta_dom_map['domain'][expl_act_name]['precondition_pos_cond'] = set()
            meta_dom_map['domain'][expl_act_name]['precondition_neg'] = set()
            meta_dom_map['domain'][expl_act_name]['effect_pos'] = set()
            meta_dom_map['domain'][expl_act_name]['effect_pos'].add(pred)
            meta_dom_map['domain'][expl_act_name]['effect_neg'] = set()
            meta_dom_map['domain'][expl_act_name]['effect_pos_cond'] = set()
            meta_dom_map['domain'][expl_act_name]['effect_neg_cond'] = set()


        #print ("Exp_ACT",meta_dom_map['domain']['explainshould_know_pickup-b2_precondition_pos_handempty'])
        #print ("act action",meta_dom_map['domain']['putdown*B'])
        #exit(0)
        return meta_dom_map



    def ground_all_operators(self, prob_object):
        ground_operators = []
        for act in prob_object.operators():
            ground_operators += list(prob_object.ground_operator(act))
        return ground_operators

    def test_failure(self, pred, plan, index):
        FAILURE_FOUND = False
        belief_pred = BELIEF_PREFIX + pred
        failure_set = set()
        #print ("index",belief_pred)
        while index >= 0 and not FAILURE_FOUND:
            #if index == -1:
            # Add initial state
            failure = None
            if pred in self.meta_model['domain'][plan[index]]['precondition_pos'] and belief_pred not in self.meta_model['domain'][plan[index]]['precondition_pos']:
                FAILURE_FOUND = True
                failure = "explain"+"should_know_"+plan[index].split('*')[0]+"_precondition_pos_"+pred.split(" ")[0]
            elif belief_pred in self.meta_model['domain'][plan[index]]['effect_pos']:
                FAILURE_FOUND = True
                failure = "believes_#"+plan[index].split('*')[0]+"#effect_pos#"+pred.split(" ")[0]
            elif pred in self.meta_model['domain'][plan[index]]['effect_neg'] :
                FAILURE_FOUND = True
                failure = "explain"+"should_know_"+plan[index].split('*')[0]+"#effect_neg_"+pred.split(" ")[0]
            if failure:
                failure_set.add(failure)
            index-=1
        return failure

    def revaluate_node(self, current_node, curr_problem):
        tmp_state = copy.deepcopy(curr_problem['problem']['init'])

        for act in current_node[1]:
            if SKIP_STATE not in tmp_state:
                precs = set()
                adds = set()
                dels = set()
                for pr in curr_problem['domain'][act]['precondition_pos']:
                    precs.add(pr)

                if not precs <= tmp_state:
                    exec_action= False
                    tmp_state = set()
                    tmp_state.add(SKIP_STATE)
                else:
                    adds = curr_problem['domain'][act]['effect_pos']
                    dels = curr_problem['domain'][act]['effect_neg']
                    tmp_state = (tmp_state| adds) - dels
        
        return [tmp_state, current_node[1], "CERT"]



    def find_all_fringe_elements(self, current_node_old, curr_problem):
        ''' 
            
        '''
        succ_list = []
        inv_succ_list = []
        if current_node_old[-1] == "UNCERT":
            current_node = self.revaluate_node(current_node_old, curr_problem)
            if SKIP_STATE in current_node[0]:
                current_node[-1] = "CERT"
                inv_succ_list.append(current_node)
                return succ_list, inv_succ_list
        else:
            current_node = current_node_old
        applicable_actions = set()
#        print ("Applicable actions", len(curr_problem['domain'].keys()))
        for act in curr_problem['domain'].keys():
            old_plan = copy.deepcopy(current_node[1])
            exec_action = True
            succ_states = []
            failed_preds = set()
            current_state = copy.deepcopy(current_node[0])
            precs = set()
            robot_precs = set()
            adds = set()
            dels = set()
            new_state = set()
            for pr in curr_problem['domain'][act]['precondition_pos']:
                precs.add(pr)

            if not precs <= current_state:
#                print ("INVALID ACTION YALL")
                exec_action= False
                new_state = set()
                new_state.add(SKIP_STATE)
                inv_succ_node = [new_state, old_plan+[act], "CERT"]
                inv_succ_list.append(inv_succ_node)
            else:
                #print (act, precs, current_state)
                model_adds = set()
                model_dels = set()


                adds = curr_problem['domain'][act]['effect_pos']
                dels = curr_problem['domain'][act]['effect_neg']

                new_state = copy.deepcopy(current_state)
                new_state = (new_state | adds) - dels


            #if exec_action:
                succ_node = [new_state, old_plan+[act], "CERT"]
                #print ("model updates", succ_node[2], succ_node[3])
                succ_list.append(succ_node)
            #else:
        return succ_list, inv_succ_list


    def find_all_successors(self, current_node):
        ''' 
            
        '''
        applicable_actions = set()
        succs = []
        #print (self.meta_model['domain'].keys())
        for act in self.meta_model['domain'].keys():
            exec_action = True
            succ_states = []
            failed_preds = set()
            current_state = copy.deepcopy(current_node[0])
            precs = set()
            robot_precs = set()
            adds = set()
            dels = set()

            for pr in self.meta_model['domain'][act]['precondition_pos']:
                precs.add(pr)
            for pr in  self.meta_model['domain'][act]['precondition_pos_cond']:
                pr_parts = pr.split('@')
                if pr_parts[0] in current_state:
                    precs.add(pr_parts[1])

            #    print (act, current_state)

            if not precs <= current_state:
                exec_action= False
            else:
                model_adds = set()
                model_dels = set()


                adds = copy.deepcopy(self.meta_model['domain'][act]['effect_pos'])
                for eff in self.meta_model['domain'][act]['effect_pos_cond']:
                    eff_parts = eff.split('@')
                    if eff_parts[0] in current_state:
                        adds.add(eff_parts[1])

                dels = copy.deepcopy(self.meta_model['domain'][act]['effect_neg'])
                for eff in self.meta_model['domain'][act]['effect_neg_cond']:
                    eff_parts = eff.split('@')
                    if eff_parts[0] in current_state:
                        dels.add(eff_parts[1])
                #print (act,"adds",adds)
                #print (act,"dels", dels)
                for pred in adds:
                    if  "believes_" in pred or "should_know_" in pred:
                        model_adds.add(pred)
                for pred in dels:
                    if  "believes_" in pred or "should_know_" in pred:
                        model_dels.add(pred)


                new_state = copy.deepcopy(current_state)
                new_state = (new_state | adds) - dels

                #new_state[2] = new_state[2]| model_adds
                #new_state[3] = new_state[3]| model_adds
                pure_plan_cost = current_node[-1]
                if 'explain' not in act:
                    pure_plan_cost +=1
                #for fail in robot_precs - current_state:
                #    failed_preds.add(fail+"#"+str(len(current_node[1])))
                if exec_action:
                    succ_node = [new_state, current_node[1]+[act], current_node[2]| model_adds, current_node[3]|model_dels, pure_plan_cost]
                    #print ("model updates", succ_node[2], succ_node[3])
                    succs.append(succ_node)
        return succs


    def search_for_optimal_plan(self, dom_map):
        global LAST_HUMAN_FRINGE, LAST_HUMAN_PLAN
        start_node = [dom_map['problem']['init'],[],"CERT"]
        LAST_HUMAN_PLAN, LAST_HUMAN_FRINGE = repeated_search(start_node, dom_map['problem']['goal'], self, dom_map, LAST_HUMAN_FRINGE, LAST_HUMAN_PLAN)

        #print (output)
        # Check for NONE 
        return len(LAST_HUMAN_PLAN[1])

    def make_problem_dom_file(self, curr_model):
        dom_file = "/tmp/unif_block_test_dom.pddl"
        prob_file = "/tmp/unif_block_test_prob.pddl"
        action_strings = []
        for act in  curr_model['domain'].keys():
                precondition_list = ['('+i+')' for i in curr_model['domain'][act]['precondition_pos']]
                precondition_list += ['(not ('+i+'))' for i in curr_model['domain'][act]['precondition_neg']]
                effect_list = ['('+i+')' for i in curr_model['domain'][act]['effect_pos']]
                effect_list += ['(not ('+i+'))' for i in curr_model['domain'][act]['effect_neg']]
                #if act == "INIT_ACT":
                #    effect_list.append("(APPLIED_"+act+')')
                #else:
                #    precondition_list.append("(APPLIED_INIT_ACT)")
                action_strings.append(ACTION_DEF_STR.format(act.replace('*','_'), "","\n".join(precondition_list),"\n".join(effect_list)))
        dom_str = self.domain_template_str.format("\n".join(action_strings))

        goal_state_str_list = ['('+i+')' for i in  curr_model['problem']['goal']]
        init_state_str_list = ['('+i+')' for i in curr_model['problem']['init']]
        prob_str = self.prob_template_str.format("\n".join(init_state_str_list), "\n".join(goal_state_str_list))

        with open(dom_file, 'w') as d_fd:
            d_fd.write(dom_str)

        with open(prob_file, 'w') as p_fd:
            p_fd.write(prob_str)
        output = os.popen(PLANNER_COMMAND.format(dom_file, prob_file)).read().strip()
        plan   = [item.strip() for item in output.split('\n')] if output != '' else []
        #print (plan)
        return len(plan) 


    def optimality_test_exact(self, current_state):
        curr_model = copy.deepcopy(self.grounded_human_model_map)
        model_key = "@".join(sorted([m for m in current_state[2]])) + "@".join(sorted([m for m in current_state[3]]))
        #print ("key:",model_key)
        if model_key in OPTIMALITY_TEST_MAP:
            return OPTIMALITY_TEST_MAP[model_key]

        model_adds = {}
        model_dels = {}
        for pred in current_state[2]:
            pred_parts = pred.split('#')
            if pred_parts[1] not in model_adds:
                model_adds[pred_parts[1]] = []
            model_adds[pred_parts[1]].append((pred_parts[2], pred))

        for pred in current_state[3]:
            pred_parts = pred.split('#')
            if pred_parts[1] not in model_dels:
                model_dels[pred_parts[1]] = []
            model_dels[pred_parts[1]].append((pred_parts[2], pred))
        #print (model_adds)
        #print (model_dels)

        for act in curr_model['domain']:
            act_name = act.split('*')[0]
            if act_name in model_adds:
                for act_part, pred in model_adds[act_name]:
                    #print ("act_part, act_pred",act_part, act_pred)
                    added_pred = None
                    for cond_pred in self.meta_model['domain'][act][act_part+'_cond']:
                        cond_pred_parts = cond_pred.split('@')
                        if cond_pred_parts[0] == pred:
                            added_pred = cond_pred_parts[1].replace(BELIEF_PREFIX,'')
                            curr_model['domain'][act][act_part].add(added_pred)
                    if not added_pred:
                        print ("something wrong!!!! update not found")
                        exit(2)
                    
                        #print ("pred_matched")

            if act_name in model_dels:
                for act_part, pred in model_dels[act_name]:
                    #print ("act_part, act_pred",act_part, act_pred)
                    added_pred = None
                    for cond_pred in self.meta_model['domain'][act][act_part+'_cond']:
                        cond_pred_parts = cond_pred.split('@')
                        if cond_pred_parts[0] == pred:
                            added_pred = cond_pred_parts[1].replace(BELIEF_PREFIX,'')
                            curr_model['domain'][act][act_part].remove(added_pred)
                    if not added_pred:
                        print ("something wrong!!!! update not found")
                        exit(2)
                    
 
        #print ("key",model_key,current_state[1])
        #self.make_problem_dom_file(curr_model)
        c = time.time()
        print ("Run inner search")
        opt_plan = self.make_problem_dom_file(curr_model)
        print ("plan time ",time.time() - orig_time)
        #opt_plan = self.search_for_optimal_plan(curr_model)
        OPTIMALITY_TEST_MAP[model_key] = opt_plan < current_state[-1]

        #print ("key:",model_key,OPTIMALITY_TEST_MAP[model_key], opt_plan, current_state[-1])
        #if model_key == 'should_know_#board#precondition_pos#lift-at@should_know_#board#precondition_pos#origin@should_know_#depart#effect_pos#served@should_know_#down#effect_pos#lift-at@should_know_#up#effect_pos#lift-at':
        #if model_key == "should_know_#board#precondition_pos#lift-at@should_know_#board#precondition_pos#origin@should_know_#depart#effect_pos#served@should_know_#depart#precondition_pos#boarded@should_know_#depart#precondition_pos#destin@should_know_#down#effect_pos#lift-at@should_know_#up#effect_neg#lift-at":
        #    exit(1)
        return OPTIMALITY_TEST_MAP[model_key]


    def find_action_cost(self, act, current_state, human_test=False):
        if 'explain' in act:
            return 1
        else:
            return 1




    def plan(self):
        current_state = [self.meta_model['problem']['init'], [],set(), set(), 0]
        current_node = astarSearch(current_state, self.meta_model['problem']['goal'], self)
        return current_node[1]


    def goal_test(self, current_state):
        #print ("goal test",self.meta_model["problem"]["goal"],current_state[0],self.meta_model["problem"]["goal"] <= current_state[0])
        if not self.meta_model["problem"]["goal"] <= current_state[0]:
            return False
        #if len(current_state[2]) > 0:
        #    return (False,current_state[2][0])
        #print ("robot", current_state[1])
        #exit(0)
        if not self.optimality_test_exact(current_state):
            return True


    def human_goal_test(self, current_state, curr_problem):

        if current_state[-1] == "UNCERT":
            current_state = self.revaluate_node(current_state, curr_problem)
        #print ("goal test",self.meta_model["problem"]["goal"],current_state[0],self.meta_model["problem"]["goal"] <= current_state[0])
        if not curr_problem["problem"]["goal"] <= current_state[0]:
            return False
        #if len(current_state[2]) > 0:
        #    return (False,current_state[2][0])
        #if not self.optimality_test_exact(current_state):
        return True
